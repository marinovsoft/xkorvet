// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once

#if defined(_MSC_VER)
  #define _CRT_SECURE_NO_WARNINGS
#endif

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h> /* for mode definitions */
#include <string.h>

#if defined(__BORLANDC__)
  #pragma option -a1
  #include <mem.h>
  #include <io.h>
  #include <ctype.h>
#elif defined(__GNUC__)
  #pragma pack(1)
#elif defined(_MSC_VER)
  #pragma pack(1)
  #include <io.h>
  #include <ctype.h>
#endif

// TODO: ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
