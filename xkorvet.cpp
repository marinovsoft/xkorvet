// xkorvet.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"

#define DEBUG

//int SectorSize=1024;
//int SectorOnTrack=5;

typedef unsigned char byte;
typedef unsigned short int word;

struct _DSKINFO /*FOLD00*/
{
  word     LoadAddr    ;// �����, � �������� ���������� ��������
  word     RunAddr     ;// �����,���� ���������� ���������� ����� ��������
  word     Count       ;// ����� ����������� ���������� ��������

  byte     SizeDisk    ;// ���� �������� ����� 1 -8" ����, ���� 0 - - 5.25"
  byte     Density     ;// 1 ������ ������: 0 - FM; 1 - MFM
  byte     TpI         ;// 1? ����� �������  �� ����: 0 - 48 TpI 1 - 96 TpI, 2 - 135 TpI
  byte     SkewFactor  ;// 1  ���� �������� ����� 1 - ������� �������� ���. ���� <> 1 - ����� 33-128 ��������������� ������� �������� ������� �������� ��������, ������ �������� ����� ����� ������� �������.
  byte     SecSize     ;// 3  ������ ����������� �������:0 - 128 ����, 1 - 256 ����, 2 - 512 ���� �  3 - 1024
  byte     InSide      ;// 1  ���������� � �������� �����: 0 - ������������� ����, 1 - �������������, ������ ������� ���������� �� 1 �� n � ������ �������, ������ ���������� ������� � ������� �������, �������� � ������.
  word     SecPerTrack ;// 5  ����� ���������� �������� �� �������
  word     TrkPerDisk  ;// 80  ����� ������� �� ����� (� ����� �������)

  word     SPT         ;// 40  ����� 128-�������� �������� �� �������
  byte     BSH         ;/* 4  ������ ������ ����� ������������� ������
                           ������������ �������� ����� ������. ����
                           ������ - ���������� ��������� ������� �����.
                           ������ ����� - 1, 2, 4, 8 ��� 16 �����. ����
                           ���� ����� ������� ������, � ������ �����
                           �������� ������������ ����� ��������������
                           ��������. ��� ���������� ������� �����
                           ������������� ������ ����������, �����������
                           ������������ ������ �����. BSH = log2[�����
                           ���������� �������� � �����]. */
  byte     BLM         ;// 15  ����� ����� ������������� ������
                        // BLM = (����� ���������� �������� � �����)-1.
  byte     EXM         ;/* 0  ����� �������
                           EXM = (BLM+1)*128/1024 - 1 - [DSM/256]
                           EXM - ��������������� �������� ��� �����������
                           ������ extent'�.
                           extent - ����� �����, ����������� �����
                           ������ � ����������. */
  word     DSM         ;// 394  ����� ������ �� ����� � ������ ����� 1
  word     DRM         ;// 127  ����� ������ � ���������� ����� ����� 1
  byte     AL0         ;// 192  ����������, ����� ����� ���������������
  byte     AL1         ;/* 0  ��� ���������� 1100000000000000b
                           ����������, ����� ����� ���������������
                           ��� ����������. ������  ��� AL0,AL1,
                           ������� �� �������� ���� AL0 � ������
                           ������� ����� AL1, ��������� 1 �����������
                           ���� ���� ������ ��� ����������. �����
                           ������������� ����������� ����� ������
                           ��� �������� ������ � ����������: 32*DRM/BLS*/
  word     CKS         ;// 32  x������ ������� �������� ����������
                        // CKS=(DRM+1)/4. ���� ���� �� �������, CKS=0
  word     OFS         ;// 2  ����� ��������� ������� �� �����

  byte     CRC         ;// ����������� ����� ��������� ���������� ��[1-31] + 66H = CRC
}; // __attribute__ ((packed));

struct _DIRENTRY   /*FOLD00*/
{
  byte     User;
  byte     Name[8];
  byte     Ext[3];
  byte     Extent;
  word     Extent2;
  byte     Record;
//  word     Cluster[8];
  byte     Cluster[16];
}; // __attribute__ ((packed));

struct _DSKINFO   DiskInfo;
struct _DIRENTRY  DIR[512];

int ClusterUseMap[1024];  //  � �-��� ����� ������� ����� ���� � ���������� ��� �����������
int UsedCluster = 0;      //  ������� ������ ������.
int UsedDirEntry = 0;     //  ������� ������ ������ � ����������.

int GetCluster (int N) /*FOLD00*/
{
// return SectorSize*SectorOnTrack*DiskInfo.OFS+(1024*2*N);
  return (128<<DiskInfo.SecSize)*DiskInfo.SecPerTrack*DiskInfo.OFS+((128<<DiskInfo.SecSize)*2*N);
}

int InitDisk(int FILE) /*FOLD00*/
{
  int CRC;
  byte BUF[sizeof(struct _DSKINFO)];
  int i;

  _lseek(FILE, 0, SEEK_SET);
  if ( (_read(FILE,BUF,sizeof(struct _DSKINFO)) != sizeof(struct _DSKINFO)) )
  {
    return -1;
  }
  memcpy((void*) &DiskInfo, (void*) BUF, 32);

  CRC=0x66;
  for (i=0; i<31; i++) CRC+=BUF[i];
  CRC&=0xFF;
  if (CRC != DiskInfo.CRC) return -2;

  _lseek(FILE,GetCluster(0),SEEK_SET);

  if ( _read(FILE,DIR,(sizeof(struct _DIRENTRY)*(DiskInfo.DRM+1)) ) !=
       (int)(sizeof(struct _DIRENTRY)*(DiskInfo.DRM+1))
     )
  {
    return -3;
  }

  return 0;
}

int FindExtent(struct _DIRENTRY *DD) /*FOLD00*/
{
  int i;

  for (i=0; i<DiskInfo.DRM+1; i++)
    if ( (memcmp((void*)DD,(void*)(&DIR[i]),15)) == 0) return i;
  return -1;
}

void usage(void)   /*FOLD00*/
{
  printf(" Usage xKorvet CMD diskname.ext {SrcFileName} {DstFileName}\n"\
         "                l  List\n"\
         "                a  Add\n"\
         "                d  Delete\n"\
         "                e  Extract SrcFileName to {DstDir}\n"\
//         "                w  Wipe - cleanup image. (Fill unused data by E5)\n"
        );
}

char *DirEntToName(struct _DIRENTRY *DD) /*FOLD00*/
{
  static unsigned char Buffer[1024];
  unsigned char *src;
  unsigned char *dst=Buffer;
  int len;
  src=DD->Name;
  len=8;
//  while ((len--) && (*src != ' ') && (*src != 0)) *dst++=*src++;
  while ((len--) && (*src != ' '))
//    if ((*src == '\0') || (*src == ':') || (*src == '>'))
  if (*src == '\0')
      *src++;
    else
      *dst++=*src++;
//  *dst++='.';
  *dst='.';
  src=DD->Ext;
  len=3;
//  while ((len--) && (*src != ' ') && (*src != 0)) *dst++=(*src++)&0x7f;
    while ((len--) && (*src != ' '))
//      if ((*src == '\0') || (*src == ':') || (*src == '>'))
      if (*src == '\0')
        *src++;
      else
        *++dst=(*src++)&0x7f;
  if (*dst == '.')
    *dst = '\0';
  else
    *++dst='\0';
  return (char*)Buffer;
}

int MatchName(char *FileName,char *SrcMask)   /*FOLD00*/
{
  char *Mask=SrcMask;
  char *Name=FileName;
  char FCBMASK[8+3+1];
  char FCB[8+3+1];
  char *tmp,*Src;
  int  i;

  _strupr(Mask);
  _strupr(Name);

//  printf("\nN:%s M:%s = ",FileName,SrcMask);

  tmp=FCBMASK;
  Src=Mask;
  // ������������ FCB  �� ����� ...
  for (i=0; i<12; i++) tmp[i]=' ';
  i=8;
  while(*Src && i && (*Src != '.'))
  {
    if (*Src == '*')
    {
      while (i--)
      {
        *tmp++='?';
      };
      Src++;
      continue;
    }
    else *tmp++=*Src++;
    i--;
  }
  if (*Src == '.')
  {
    Src++;
    if (i>0) while (i--)
      {
        *tmp++=' ';
      };
  }
  i=3;
  while(*Src && i)
  {
    if (*Src == '*')
    {
      while (i--)
      {
        *tmp++='?';
      };
      Src++;
      continue;
    }
    else *tmp++=*Src++;
    i--;
  }

  // ������������ FCB  �� ��������� ����� �����
  tmp=FCB;
  Src=Name;
  for (i=0; i<12; i++) tmp[i]=' ';
  i=8;
  while(*Src && i && (*Src != '.'))
  {
    if (*Src == '*')
    {
      while (i--)
      {
        *tmp++='?';
      };
      Src++;
      continue;
    }
    else *tmp++=*Src++;
    i--;
  }
  if (*Src == '.')
  {
    Src++;
    if (i>0) while (i--)
      {
        *tmp++=' ';
      };
  }
  i=3;
  while(*Src && i)
  {
    if (*Src == '*')
    {
      while (i--)
      {
        *tmp++='?';
      };
      Src++;
      continue;
    }
    else *tmp++=*Src++;
    i--;
  }

  // �������� ����� � ����

  for (i=0; i<12; i++)
  {
    if (FCBMASK[i] != '?')
      if (FCBMASK[i] != FCB[i]) return 0;
  }
  return 1;
}



int isValidDirRecord(struct _DIRENTRY *DD) // ��������� ���������� ����� � ���������� /*FOLD00*/

{
  int i;

//  byte     User;
//  byte     Name[8];
//  byte     Ext[3];
//  byte     Extent;
//  word     Extent2;
//  byte     Record;
//  word     Cluster[8];


  if ( DD->User == 0xE5 ) return 1; // Deleted Record (EMPTY)

//  if ( (DD->User > 10 ) && (DD->User != 0xe5) ) {}; // invalid user

//  for (i=0;i<8;i++) if (DD->Name[i] < 0x20) return -1;
//  for (i=0;i<3;i++) if (DD->Ext[i] < 0x20) return -1;

  if (DD->Extent > 0x1f)
  {
    printf("e:ext ");  // To HIGH extent
    return 0;
  }
  if (DD->Record > 0x80)
  {
    printf("e:rec ");  // To HIGH col of Record
    return 0;
  }
  for (i=0; i<8; i++)
    if (DiskInfo.DSM > 0xFF)
    {
      if (*(unsigned short int*)&(DD->Cluster[i << 1]) > DiskInfo.DSM+1)
      {
        printf("e:cls ");
        return 0;
      }
    }
    else if (DD->Cluster[i] > DiskInfo.DSM+1)
    {
      printf("e:cls ");
      return 0;
    }
  return 1;
}

int FindFreeCluster(void)   /*FOLD00*/
{
  int i;
  for(i=2; i<DiskInfo.DSM+1; i++) if (ClusterUseMap[i] == -1) return i;
  return -1;
}

int AddFileToDisk(int FileKDI,char *FileName,int User)   /*FOLD00*/
{
  int AddFile;
  int FileLen;
  int ClusterCnt;
  int DirEntryCnt;
  unsigned char BUF[2048];
  int i,j,k,r;
  struct _DIRENTRY FCB;
  char *src;
  int WriteLen;

// prepare file name
  src=FileName;
  FCB.User=User;
  i=0;

  AddFile=_open(FileName,O_RDONLY|O_BINARY);
  if (AddFile<0)
  {
    printf("Err: can't open file %s\n",FileName);
    return -1;
  }

  FileLen=_lseek(AddFile,0,SEEK_END);

  ClusterCnt=FileLen/((128<<DiskInfo.SecSize)*2); // /ClusterSize

  DirEntryCnt=ClusterCnt/8+1;

  printf("dbg: FileLen %d\n",FileLen);

  if (ClusterCnt>(DiskInfo.DSM+1-UsedCluster))
  {
    printf("Err: NoEnoughtFreeCluster ");
    return -1;
  }
  printf("Cluster need: %d\n",ClusterCnt);

  if (DirEntryCnt>(DiskInfo.DRM+1-UsedDirEntry))
  {
    printf("Err: NoFreeDirEntry");
    return -1;
  }
  printf("DirEntry need: %d\n",DirEntryCnt);

// Prepare DIR entry
  /*
    struct _DIRENTRY {
      byte     User;
      byte     Name[8];
      byte     Ext[3];
      byte     Extent;
      word     Extent2;
      byte     Record;
      word     Cluster[8];
    };
  */
  _lseek(AddFile,0,SEEK_SET); // rewind source file

  while ((i<8) && (*src) && (*src != '.')) FCB.Name[i++]=*src++;
  while (i<8) FCB.Name[i++]=' ';
  if (*src == '.') src++;
  i=0;
  while ((i<3) && (*src) ) FCB.Ext[i++]=*src++;
  while (i<3) FCB.Ext[i++]=' ';

  FCB.Extent=0;
  FCB.Extent2=0;

  while (FileLen)
  {
    for (i=0; i<8; i++)
      if (DiskInfo.DSM > 0xFF)
        *(unsigned short int*)(&FCB.Cluster[i << 1])=0;
      else
        FCB.Cluster[i]=0;
    FCB.Record=0;
    j=0;
    for (i=0; i<DiskInfo.DRM+1; i++)
      if ( DIR[i].User == 0xE5 ) break;
    // I= free dir entry
    while ((FileLen) && (j < 8))
    {
      k=FindFreeCluster(); // check if -1
      if (FileLen>=2048)
      {
        FCB.Record+=2048/128;
        WriteLen=2048;
      }
      else
      {
        FCB.Record+= (FileLen / 128);
//      if (FileLen & 0xff) FCB.Record++;
        if (FileLen & 0x7F) FCB.Record++;
// ~~~ esli fajl tochno raven !!!!!!!!!!!!!!!!!!!
        WriteLen=FileLen;
      }
      memset(BUF,0xE5,2048);
      r=_read(AddFile,BUF,WriteLen);
// printf("rr2k: %d",r);
      _lseek(FileKDI,GetCluster(k),SEEK_SET);
      r=_write(FileKDI,BUF,2048); // write cluster to KDI
// printf("rw2k: %d",r);
      FileLen-=WriteLen;
      if (DiskInfo.DSM > 0xFF)
        *(unsigned short int*)(&FCB.Cluster[j++ << 1])=k;
      else
        FCB.Cluster[j++]=k;
      ClusterUseMap[k]=i;
    } // in
    memcpy(
      (void *) &DIR[i],
      (void *) &FCB,
      sizeof (FCB) ); // update DIR entry

    if (FCB.Extent != 0x1f) FCB.Extent++;
    else
    {
      FCB.Extent=0;
      FCB.Extent2+=0x100;
    }
  } // while (filelen)

// write DIR to KDI
  _lseek(FileKDI,GetCluster(0),SEEK_SET);
  r=_write(FileKDI,DIR,(sizeof(struct _DIRENTRY)*(DiskInfo.DRM+1)));
// printf("rwdir: %d",r);

  _close(AddFile);

  return 0;
}


void PrepareForWrite(void)   /*FOLD00*/
{
  int i,j;
  int C1,C2;

  UsedCluster=0;
  UsedDirEntry=0;
  for(i=0; i<DiskInfo.DSM+1; i++) ClusterUseMap[i]=-1;

  for (i=0; i<DiskInfo.DRM+1; i++)
  {
    if ( isValidDirRecord(&DIR[i]))
    {
      if (DIR[i].User != 0xE5)
      {
        UsedDirEntry++;
        for (j=0; j<((DIR[i].Record-1)>>4)+1; j++)
          if (DiskInfo.DSM > 0xFF)
          {
            if (ClusterUseMap[*(unsigned short int*)(&DIR[i].Cluster[j << 1])] != -1)
            {
              C1=ClusterUseMap[*(unsigned short int*)(&DIR[i].Cluster[j << 1])];
              printf("!> crosslink: cluster %d use file %s ",
                     *(unsigned short int*)(&DIR[i].Cluster[j << 1]), DirEntToName(&DIR[i]));
              printf("and file %s\n",DirEntToName(&DIR[C1]));
            }
            else
            {
              ClusterUseMap[*(unsigned short int*)(&DIR[i].Cluster[j << 1])]=i;
              UsedCluster++;
            }
          } // in DIR[i] cluseter loop
          else
          {
            if (ClusterUseMap[DIR[i].Cluster[j]] != -1)
            {
              C1=ClusterUseMap[DIR[i].Cluster[j]];
              printf("!> crosslink: cluster %d use file %s ",
                     DIR[i].Cluster[j],DirEntToName(&DIR[i]));
              printf("and file %s\n",DirEntToName(&DIR[C1]));
            }
            else
            {
              ClusterUseMap[DIR[i].Cluster[j]]=i;
              UsedCluster++;
            }
          } // in DIR[i] cluseter loop
      } // if E5
    }
    else
    {
      printf("Invalid dir entry %d\n",i);
    }
  } // DIR loop
  printf("Cluster>\tTotal: %4d\tUsed: %4d\tFree: %4d\n",
         DiskInfo.DSM+1,
         UsedCluster,
         DiskInfo.DSM+1-UsedCluster);
  printf("Dir    >\tTotal: %4d\tUsed: %4d\tFree: %4d\n",
         DiskInfo.DRM+1,
         UsedDirEntry,
         DiskInfo.DRM+1-UsedDirEntry);
}


void WipeDisk(char *FileName)
{

  int i;
  unsigned char BUF[2048];
  int DirFree=0;
  int ClusterFree=0;
  int F;

  memset(BUF,0xE5,2048);

  PrepareForWrite();

  F=_open(FileName,O_RDWR|O_BINARY);

// wipe empty directory
  for (i=0; i<DiskInfo.DRM+1; i++)
    if (DIR[i].User == 0xE5)
    {
      memset(&DIR[i], 0xE5, sizeof(struct _DIRENTRY));
      DirFree++;
    }
//wipe free clusters
  for(i=2; i<DiskInfo.DSM+1; i++)
    if (ClusterUseMap[i] == -1)
    {
      _lseek(F,GetCluster(i),SEEK_SET);
      _write(F,BUF,2048); // write cluster to KDI
      ClusterFree++;
    }

  if (DirFree)
  {
    _lseek(F,GetCluster(0),SEEK_SET);
    _write(F,DIR,(sizeof(struct _DIRENTRY)*(DiskInfo.DRM+1)));
  }

  printf("Wiped:\r\nDirectory Entry - %5d\r\nCluster         - %5d\r\n",DirFree,ClusterFree);
  _close(F);

}

int main(int argc, char *argv[]) /*FOLD00*/
{
  int    i,j,k;
  int    size;
  word   CL[1024];
  int    CLN;
  int    N;
  int    Ext;
  int    F;
  struct _DIRENTRY D;
  char   DstPath[1024];
  char   FileMask[1024];
  char   NAME[1024];
  FILE  *FOUT;
  int    BlockSize;
  char   BUF[10*1024];

  char  *CMD;
  char  *tmp;

  printf("\nxKorvet Version 1.3f5, copyright 2002-3 by Sergey Erokhin\n");
//        "Universe, Solar System, Earth, Europe, Ukraine, Kharkov\n");


#ifdef DEBUG
  for (i=0; i<argc; i++) printf("argv[%d]=%s\n",i,argv[i]);
#endif


  if (argc < 3)
  {
    usage();
    return -1;
  };

  CMD=argv[1];

  if (argc > 3)
    strcpy(FileMask,argv[3]);
  else
    strcpy(FileMask,"*.*");

  if (FileMask[0] == '\\')    // FIX \filename.ext path
  {
    strcpy(FileMask,FileMask+1);
  }

  D.User = 0;
  if ( strchr(FileMask,'\\') )
  {

    if ((!isdigit(FileMask[0]) || !isdigit(FileMask[1])) & (FileMask[0]!='E' || FileMask[1]!='5'))
    {
      printf("ERROR !!!!, invalid User. !!!!!!!!!!!!!\n\r");
      exit(-1);
    }
    else
    {
      if (FileMask[0] == 'E' && FileMask[1] == '5')
        D.User = 0xE5;
      else
        D.User = (FileMask[0]-0x30)*10+(FileMask[1]-0x30);
      printf("User: %C%C\r\n",FileMask[0],FileMask[1]);
      strcpy(FileMask,FileMask+3);
    }
  }


#ifdef DEBUG
  printf("CMD: %s\nFileName: %s\n",CMD,argv[2]);
#endif

  if (*CMD == 'e')
  {
    if (argc == 5)
    {
      strcpy(DstPath,argv[4]);
    }
    else if (strchr(FileMask,':') || strchr(FileMask,'\\'))     // parser: e Name Path
    {
      strcpy(DstPath,FileMask);
      strcpy(FileMask,"*.*");
    }
    else
    {
      strcpy(DstPath,".");
    }

    i=strlen(DstPath);
    if (DstPath[i-1] == '\\') DstPath[i-1]='\0';
    strcat(DstPath,"\\");
    printf("DstPath: %s\n",DstPath);
  }
#ifdef DEBUG
  printf("FileMask: %s\n",FileMask);
#endif

// Open KDI

  printf("KDI: %s\n",argv[2]);

  F=_open(argv[2],O_RDWR|O_BINARY);

  if (F < 0)
  {
    printf("error open file %s\n",argv[1]);
    return(0);
  }
  if (InitDisk(F)<0)
  {
    printf("error info sector CRC.\n");
    return(0);
  }

  if (*CMD == 'a')
  {
//   if (D.User != 0xE5) {
    if (D.User <  0x20)
    {
      printf("ADD:\n");
      PrepareForWrite();
      _strupr(FileMask);
      AddFileToDisk(F, FileMask, D.User);
//   AddFileToDisk(F,argv[3]);
//   AddFileToDisk(F,"a");
    }
    _close(F);
    return 0;
  }

  if (*CMD == 'd' && D.User == 0xE5)
  {
    printf("Invalid command for 0xE5 user");
    return 0;
  }

  if (*CMD == 'l')  printf("Begin:\n");

  for (i=0; i<DiskInfo.DRM+1; i++)
  {
    CLN=0;
    size=0;
//    Ext=0;

    if ( (DIR[i].Extent == 0) && (DIR[i].Extent2 == 0))
    {
      if ( ((*CMD == 'e') || (*CMD == 'd')) && (D.User != DIR[i].User)) continue;
      memcpy((void*)&D,(void*)&(DIR[i]),32);

      if (!MatchName(DirEntToName(&D),FileMask)) continue;
//      if ( ((*CMD == 'e') || (*CMD == 'd')) && (D.User != DIR[N].User)) continue;

      while ((N=FindExtent(&D)) >= 0)
      {
        size+=DIR[N].Record;
        for (j=0; j<((DIR[N].Record-1)>>4)+1; j++)
          if (DiskInfo.DSM > 0xFF)
            CL[CLN++]=*(unsigned short int*)(&DIR[N].Cluster[j << 1]);
          else
            CL[CLN++]=DIR[N].Cluster[j];
        if (D.Extent != 0x1f) D.Extent++;
        else
        {
          D.Extent=0;
          D.Extent2+=0x100;
        }

        if (*CMD == 'd') DIR[N].User=0xE5; // DELETE File
//        Ext++;
      };

      if (*CMD == 'l')
      {
        j=0;
        if (((DIR[i].User>=0) && (DIR[i].User<=31)) || (DIR[i].User==0xE5))
        {
            int isEmpty = 1;
            for (int jj=0;jj<8 && isEmpty;jj++)
              if ((DIR[i].Name[jj]!=' ') && (DIR[i].Name[jj]!=0))
                isEmpty = 0;

            for (int jj=0;jj<2 && isEmpty;jj++)
              if ((DIR[i].Ext[jj]!=' ') && (DIR[i].Ext[jj]!=0))
                isEmpty = 0;

            if (!isEmpty)
            {
              if ((DIR[i].User>=1) && (DIR[i].User<=31))
              {
                printf("%02i\\",DIR[i].User);
                j+=3;
              }
              if (DIR[i].User==0xE5)
              {
                printf("E5\\");
                j+=3;
              }
              printf ("%-12s%s %-7d\n",DirEntToName(&DIR[i]),"         "+j,size*128);
            }
        }
      }
      else if (*CMD == 'e')
      {
        sprintf(NAME,"%s%s",DstPath,DirEntToName(&DIR[i]));
        printf ("name:%s\tdst:%s\n",DirEntToName(&DIR[i]),NAME);
        if ((FOUT=fopen(NAME,"wb"))!=0) {
          for (k=0; k<CLN; k++)
          {
            _lseek(F,GetCluster(CL[k]),SEEK_SET);
            BlockSize=(size>16)?16*128:size*128;
            _read(F,BUF,BlockSize);
            fwrite(BUF,BlockSize,1,FOUT);
            size-=16;
          }
        fclose(FOUT);
        }
        else
          printf("Error create file %s\n", NAME);
      }
    }
  }
  if (*CMD == 'l') printf("End.\n");

  if (*CMD == 'd')
  {
    _lseek(F,GetCluster(0),SEEK_SET);
    _write(F,DIR,(sizeof(struct _DIRENTRY)*(DiskInfo.DRM+1)));
  }

  _close(F);

  if (*CMD == 'w' ) WipeDisk(argv[2]);
  return 0;
}
